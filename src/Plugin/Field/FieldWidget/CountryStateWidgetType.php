<?php

namespace Drupal\country_state\Plugin\Field\FieldWidget;

use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\FieldItemListInterface;

/**
 * Plugin implementation of the 'country_state_widget_type' widget.
 *
 * @FieldWidget(
 *   id = "country_state_widget_type",
 *   module = "country_state",
 *   label = @Translation("Country state widget type"),
 *   field_types = {
 *     "country_state_field_type"
 *   }
 * )
 */
class CountryStateWidgetType extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'size' => 60,
      'placeholder' => '',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    // $summary[] = t('Textfield size: @size', [
    // '@size' => $this->getSetting('size'),
    // ]);
    if (!empty($this->getSetting('placeholder'))) {
      $summary[] = $this->t('Placeholder: @placeholder', ['@placeholder' => $this->getSetting('placeholder')]);
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {

    $full_country_list = $this->service('address.country_repository')->getList();
    $item = $items->get($delta);

    $element += [
      '#type' => 'details',
      '#open' => TRUE,
    ];

    $element['country'] = [
      '#type' => 'select',
      '#title' => $this->t('Country'),
      '#empty_option' => "- Select - ",
      '#options' => $full_country_list,
      '#default_value' => $item->country ?? 'AF',
      '#ajax' => [
        'callback' => [$this, 'getStates'],
        'wrapper' => 'states-to-update-' . $delta,
        'progress' => [
          'type' => 'throbber',
          'message' => 'Please wait...',
        ],
      ],
    ];

    $default_state = unserialize($item->state, ['allowed_classes' => FALSE]);
    $options_state = $this->service('address.subdivision_repository')->getList([$item->country ?? AF]);

    $element['state'] = [
      '#title' => $this->t('State'),
      '#type' => 'select',
      '#description' => $this->t('Select the state'),
      '#prefix' => '<div id="state-div">',
      '#suffix' => '</div>',
      '#options' => $options_state,
      '#default_value' => $default_state,
      '#attributes' => ["id" => 'states-to-update-' . $delta],
      '#multiple' => TRUE,
      '#validated' => TRUE,
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function getStates(array &$element, FormStateInterface $form_state) {
    $triggeringElement = $form_state->getTriggeringElement();
    $value = $triggeringElement['#value'];
    $states = $this->getStatesByCountry($value);
    $wrapper_id = $triggeringElement["#ajax"]["wrapper"];
    $renderedField = '';
    foreach ($states as $key => $value) {
      $renderedField .= "<option value='" . $key . "'>" . $value . "</option>";
    }
    $response = new AjaxResponse();
    $response->addCommand(new HtmlCommand("#" . $wrapper_id, $renderedField));
    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public function getStatesByCountry($default_country) {
    // Logic return states by country.
    $states = $this->service('address.subdivision_repository')->getList([$default_country]);
    return $states;
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    // Prevent Error.(This value should be of the correct primitive type.).
    foreach ($values as $key => $value) {
      $values[$key]['state'] = serialize($values[$key]['state']);
    }
    return $values;
  }

}
